FROM ubuntu:20.04

WORKDIR /src
ADD https://github.com/hadolint/hadolint/releases/download/v2.10.0/hadolint-Linux-x86_64 .

RUN mv /src/hadolint-Linux-x86_64 /usr/local/bin/hadolint
RUN chmod +x /usr/local/bin/hadolint

ENTRYPOINT [""]